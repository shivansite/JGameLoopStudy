package com.shivandragon.jGameLoopStudy.common;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;


public class BallClockTools {
    
    private static BufferedImage[] animImages;
    
    static {
        animImages = new BufferedImage[10];
        
        File imgsDir = new File(new File("."), "/res/ball_clock_anim");
        
        for(int i=1; i<=10; i++) {
            String imgIdxStr = i<10 ? "0"+i : ""+i;
            File imgFile = new File(imgsDir, String.format("ball_clock_anim_%s.png", new Object[]{imgIdxStr}));
            try {
                animImages[i-1] = ImageIO.read(imgFile);
            } catch (IOException e) {
                e.printStackTrace();
            } 
        }
    }
    
    public static BufferedImage[] loadAnimationImages() {
       return animImages;
    }
    
    public static void main(String[] args) {
        BufferedImage[] animImages = loadAnimationImages();
        for(BufferedImage animImage:animImages) {
            if(animImage==null) {
                throw new RuntimeException("Shouldn't be null");
            }
        }
        
        System.out.println("ok");
    }
}
