package com.shivandragon.jGameLoopStudy.common;

import javax.swing.JLabel;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class MeterPanel extends JPanel {
    
    private JLabel upsLabel;
    
    private String title;
    
    private boolean started;
    private long st,et;
    private int upsCount;
    
    public MeterPanel(String title) {
        this.title = title;
        upsLabel = new JLabel();
        upsLabel.setText(title+": ???");
        this.add(upsLabel);
    }
    
    public void onUpdate() {
        upsCount++;
        if(!started) {
            st = System.currentTimeMillis();
            started=true;
        } else {
            et = System.currentTimeMillis();
            if(et-st>500) {
               float ups = (upsCount*1000)/(float)(et-st);
               String upsStr = String.format("%.1f", ups);
               upsLabel.setText(title+": "+upsStr);
               upsCount = 0;
               st = et;
            }
        }
    }
}
