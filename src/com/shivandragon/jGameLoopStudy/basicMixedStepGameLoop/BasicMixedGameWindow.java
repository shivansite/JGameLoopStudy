package com.shivandragon.jGameLoopStudy.basicMixedStepGameLoop;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JPanel;

import com.shivandragon.jGameLoopStudy.basicMixedStepGameLoop.BasicMixedGameEntity.Sprite;
import com.shivandragon.jGameLoopStudy.common.MeterPanel;


@SuppressWarnings("serial")
public class BasicMixedGameWindow extends JFrame {

    private static final String TITLE_PREFIX = "Bsc Mixed Step Game Loop-";
    
    private static final int WINDOW_SIZE = 500;
    
    private JPanel gamePanel;
    private MeterPanel stateUpsMeterPanel;
    private MeterPanel framesUpsMeterPanel;
    
    private BasicMixedGameEntity[] protoGameEntities;
    
    private int stateUpdatesCount;
    private int framesUpdatesCount;
    
    private long lastTimeDisplayedFrameSkip;
    
    private boolean skippedFrame;
        
    public BasicMixedGameWindow(BasicMixedGameEntity... protoGameEntities) {
        gamePanel = new JPanel();
        gamePanel.setPreferredSize(new Dimension(WINDOW_SIZE, WINDOW_SIZE));
        gamePanel.setBackground(Color.GRAY);
        
        stateUpsMeterPanel = new MeterPanel("UPS");
        gamePanel.add(stateUpsMeterPanel);
        
        framesUpsMeterPanel = new MeterPanel("FPS");
        gamePanel.add(framesUpsMeterPanel);
                
        this.getContentPane().add(gamePanel);
        this.pack();
        this.setVisible(true);
        this.setResizable(false);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setTitle(TITLE_PREFIX);
        
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width-this.getSize().width-dim.width/80, dim.height-this.getSize().height-dim.height/15);
        
        this.protoGameEntities = protoGameEntities;
        lastTimeDisplayedFrameSkip = System.currentTimeMillis();
        
        
    }
    
    public void updateState() {
        stateUpsMeterPanel.onUpdate();
        for(BasicMixedGameEntity entity:protoGameEntities) {
            entity.update();            
        }
        stateUpdatesCount++;
        updateSkippedFramesDisplay();
    }
    
    public void updateGraphics() {
        framesUpsMeterPanel.setBackground(Color.white);        
        framesUpsMeterPanel.onUpdate();
        
        for(BasicMixedGameEntity entity:protoGameEntities) {
            Sprite sprite = entity.getCurrentSprite();
            gamePanel.getGraphics().drawImage(sprite.image, sprite.x, sprite.y, sprite.width, sprite.height, null);
        } 
        
        framesUpdatesCount++;
        skippedFrame = false;
    }
    
    private void updateSkippedFramesDisplay() {
        if(skippedFrame) {
            framesUpsMeterPanel.setBackground(Color.red);
        } else {
            framesUpsMeterPanel.setBackground(Color.white);
        }
        
        if(System.currentTimeMillis()-lastTimeDisplayedFrameSkip>500) {
            float framesSkippedPercent = 100f - (framesUpdatesCount/(float)stateUpdatesCount)*100f;
            framesUpdatesCount = 0;
            stateUpdatesCount = 0;
            String framesSkippedPercentStr = String.format("%.1f", Math.max(framesSkippedPercent,0));
            this.setTitle(TITLE_PREFIX+"Skipped frames: "+framesSkippedPercentStr+" %");
            lastTimeDisplayedFrameSkip = System.currentTimeMillis();
        }
        
        skippedFrame = true;
    }
}
