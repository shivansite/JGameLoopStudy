package com.shivandragon.jGameLoopStudy.basicMixedStepGameLoop;

import java.awt.image.BufferedImage;

import com.shivandragon.jGameLoopStudy.basicFixedStepGameLoop.BasicFixedDebug;
import com.shivandragon.jGameLoopStudy.common.BallClockTools;


public class BasicMixedGameEntity {


    private BufferedImage[] animationImages;
    
    private int currentAnimationImageIdx;
    private int loopsSinceLastImageIdxChanged;
    
    private int startX, startY;
    private int width, height;
    
    private final String tag;
    
    private boolean simulateGraphicsUpdateDelay;
    private boolean simulateStateUpdateDelay;
    
    private final int updatesInASecond;
    
    private BasicFixedDebug debugger;
    
    public BasicMixedGameEntity(String tag, int startX, int startY, int width, int height, boolean simulateGraphicsUpdateDelay, boolean simulateStateUpdateDelay) {
        animationImages = BallClockTools.loadAnimationImages();
        this.startX = startX;
        this.startY = startY;
        this.width = width;
        this.height = height;
        this.tag = tag;
        this.updatesInASecond = 1000/BasicMixedGameLoop.FIXED_STEP_IDEAL_DURATION_MS;
        debugger = new BasicFixedDebug();
        this.simulateGraphicsUpdateDelay = simulateGraphicsUpdateDelay;
        this.simulateStateUpdateDelay = simulateStateUpdateDelay;
    }
    
    public BasicMixedGameEntity(String tag, int startX, int startY, int width, int height) {
        this(tag, startX, startY, width, height, false, false);
    }
    
    public void update() {
        if(simulateStateUpdateDelay && currentAnimationImageIdx==7) {
            sleep(40);
        }
        loopsSinceLastImageIdxChanged = loopsSinceLastImageIdxChanged+1;
        if(loopsSinceLastImageIdxChanged==updatesInASecond) {
            currentAnimationImageIdx++;
            currentAnimationImageIdx = currentAnimationImageIdx%10;//wrap animation around
            loopsSinceLastImageIdxChanged=0;
        }
        
        debugger.debug(tag, currentAnimationImageIdx);
    }
    
    Sprite getCurrentSprite() {
        if(simulateGraphicsUpdateDelay && currentAnimationImageIdx==7) {
            sleep(40);
        }
        return new Sprite(this.startX, this.startY,
            this.width, this.height,
            animationImages[currentAnimationImageIdx]);
    }
    
    public class Sprite {
        public final int x,y,width,height;
        public final BufferedImage image;
        
        public Sprite(int x, int y, int width, int height, BufferedImage image) {
            this.x = x;
            this.y = y;
            this.width = width;
            this.height = height;
            this.image = image;
        }        
    }
    
    private void sleep(long ms) {
        try {
            Thread.sleep(ms);
        } catch(Throwable t) {}
    }
    
}
