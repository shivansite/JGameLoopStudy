package com.shivandragon.jGameLoopStudy.basicMixedStepGameLoop;




public class BasicMixedGameLoop {
    
    public static final int FIXED_STEP_IDEAL_DURATION_MS = 1000/25; //(hope for) 25 updates per second
//    public static final int FIXED_STEP_IDEAL_DURATION_MS = 1000/100; //(hope for) 100 updates per second
//    public static final int FIXED_STEP_IDEAL_DURATION_MS = 1000/200; //(hope for) 200 updates per second
    
    
    public static void start() throws InterruptedException {
        BasicMixedGameEntity ballClockGameEntity1 = new BasicMixedGameEntity("Clock1", 0,0, 128, 128);
        BasicMixedGameEntity ballClockGameEntity2 = new BasicMixedGameEntity("Clock2", 50,200, 150, 150);
        BasicMixedGameEntity ballClockGameEntity3 = new BasicMixedGameEntity("Clock3", 300,200, 110, 110);
        
//        BasicMixedGameEntity ballClockGameEntity4 = new BasicMixedGameEntity("Clock4", 300,100, 120, 120);
        
        //simulate delay in graphics "rendering":
//        BasicMixedGameEntity ballClockGameEntity4 = new BasicMixedGameEntity("Clock4", 300,100, 120, 120, true, false);
        
        //simulate delay in state update:
        BasicMixedGameEntity ballClockGameEntity4 = new BasicMixedGameEntity("Clock4", 300,100, 120, 120, false, true);
       
        BasicMixedGameWindow mainWindow = new BasicMixedGameWindow(ballClockGameEntity1 
            ,ballClockGameEntity2,
            ballClockGameEntity3,
            ballClockGameEntity4
            );
        
        long accumulatedTimeMs = 0;
        while(true) {
            long startTime = System.currentTimeMillis();
            
            mainWindow.updateGraphics();
            while(accumulatedTimeMs >= FIXED_STEP_IDEAL_DURATION_MS) {
                mainWindow.updateState();
                accumulatedTimeMs -= FIXED_STEP_IDEAL_DURATION_MS;
            }
            
            long endTime = System.currentTimeMillis();
            accumulatedTimeMs += endTime - startTime;
        }
    }
    
    public static void main(String[] args) throws InterruptedException {
        start();
    }
    
}
