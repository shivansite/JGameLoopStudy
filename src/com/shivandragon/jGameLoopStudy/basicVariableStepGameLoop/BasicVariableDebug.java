package com.shivandragon.jGameLoopStudy.basicVariableStepGameLoop;

public class BasicVariableDebug {

    private int lastAnimationImageIdx;
    private boolean started;
    @SuppressWarnings("unused")
    private long lastIndexLifeStartMs;
    
    private int idxChangeCount;
    private long startTimeMs;
    
    public void debug(String gameEntityTag, int currentAnimationImageIdx) {
        if(!started) {
            lastIndexLifeStartMs = System.currentTimeMillis();
            startTimeMs = System.currentTimeMillis();
            started = true;
        }
        if(currentAnimationImageIdx!=lastAnimationImageIdx) {
//            System.out.println(gameEntityTag+": "+(lastAnimationImageIdx+1)+" was actually displayed for "+(System.currentTimeMillis()-lastIndexLifeStartMs)+" ms");
            if(currentAnimationImageIdx>lastAnimationImageIdx) {
                idxChangeCount = idxChangeCount + currentAnimationImageIdx-lastAnimationImageIdx;
            } else {
                idxChangeCount = idxChangeCount + 10 - lastAnimationImageIdx + currentAnimationImageIdx;
            }
            lastAnimationImageIdx = currentAnimationImageIdx;
            lastIndexLifeStartMs = System.currentTimeMillis();
            if(idxChangeCount%5==0) {
                debugOverallPrecision(gameEntityTag);
            }
        }
        
    }
    
    private void debugOverallPrecision(String gameEntityTag) {
        float actualSecondsPassed = (System.currentTimeMillis()-startTimeMs)/1000f;
//        float precision = (idxChangeCount*100f)/actualSecondsPassed;
        float precision = (Math.min(idxChangeCount,actualSecondsPassed)*100f)/Math.max(idxChangeCount,actualSecondsPassed);
        System.out.println(gameEntityTag+": "+"counted "+idxChangeCount+" seconds in "+(actualSecondsPassed)+" actual seconds (precision is now "+precision+" % )");
    }
    
}
