package com.shivandragon.jGameLoopStudy.basicVariableStepGameLoop;



public class BasicVariableGameLoop {

    public static void start() {
        BasicVariableGameEntity ballClockGameEntity1 = new BasicVariableGameEntity("Clock1", 0,0, 128, 128);
        BasicVariableGameEntity ballClockGameEntity2 = new BasicVariableGameEntity("Clock2", 50,200, 150, 150);
        BasicVariableGameEntity ballClockGameEntity3 = new BasicVariableGameEntity("Clock3", 300,200, 110, 110);
        BasicVariableGameEntity ballClockGameEntity4 = new BasicVariableGameEntity("Clock4", 300,100, 120, 120, true);
        BasicVariableGameWindow mainWindow = new BasicVariableGameWindow(ballClockGameEntity1, 
            ballClockGameEntity2,
            ballClockGameEntity3,
            ballClockGameEntity4);
                
        long deltaTimeMs = 0;
        while(true) {
            long st = System.currentTimeMillis();
            mainWindow.update(deltaTimeMs);
            long et = System.currentTimeMillis();
            deltaTimeMs = et-st;
        }
    }
    
    public static void main(String[] args) {
        start();
    }
    
}
