package com.shivandragon.jGameLoopStudy.basicVariableStepGameLoop;

import java.awt.image.BufferedImage;

import com.shivandragon.jGameLoopStudy.common.BallClockTools;


public class BasicVariableGameEntity {


    private BufferedImage[] animationImages;
    
    private int currentAnimationImageIdx;
    private long accumulatedDeltaTimeMs;
    
    private int startX, startY;
    private int width, height;
    
    private final String tag;
    
    private boolean simulateUpdateDelay;
    
    private BasicVariableDebug debugger;
    
    public BasicVariableGameEntity(String tag, int startX, int startY, int width, int height, boolean simulateUpdateDelay) {
        animationImages = BallClockTools.loadAnimationImages();
        this.startX = startX;
        this.startY = startY;
        this.width = width;
        this.height = height;
        this.tag = tag;
        debugger = new BasicVariableDebug();
        this.simulateUpdateDelay = simulateUpdateDelay;
    }
    
    public BasicVariableGameEntity(String tag, int startX, int startY, int width, int height) {
        this(tag, startX, startY, width, height, false);
    }
    
    public void update(long deltaTimeMs) {
        accumulatedDeltaTimeMs+=deltaTimeMs;
        int animImageIncremIdx = (int) (accumulatedDeltaTimeMs/(1000f));
        
        if(animImageIncremIdx > 0) {
            currentAnimationImageIdx = currentAnimationImageIdx + animImageIncremIdx;
            currentAnimationImageIdx = currentAnimationImageIdx%10;//wrap animation around
            accumulatedDeltaTimeMs = accumulatedDeltaTimeMs-animImageIncremIdx*1000;
        }
        debugger.debug(tag, currentAnimationImageIdx);
    }
    
    Sprite getCurrentSprite() {
        if(simulateUpdateDelay && currentAnimationImageIdx==7) {
            try {
                Thread.sleep(50);
            } catch(Throwable t) {}
        } 
        return new Sprite(this.startX, this.startY,
            this.width, this.height,
            animationImages[currentAnimationImageIdx]);
    }
    
    public class Sprite {
        public final int x,y,width,height;
        public final BufferedImage image;
        
        public Sprite(int x, int y, int width, int height, BufferedImage image) {
            this.x = x;
            this.y = y;
            this.width = width;
            this.height = height;
            this.image = image;
        }        
    }
    
}
