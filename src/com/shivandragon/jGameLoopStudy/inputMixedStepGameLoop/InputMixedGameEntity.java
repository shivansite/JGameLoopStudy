package com.shivandragon.jGameLoopStudy.inputMixedStepGameLoop;

import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;

import com.shivandragon.jGameLoopStudy.basicFixedStepGameLoop.BasicFixedDebug;
import com.shivandragon.jGameLoopStudy.common.BallClockTools;


public class InputMixedGameEntity {


    private BufferedImage[] animationImages;
    
    private int currentAnimationImageIdx;
    private int loopsSinceLastImageIdxChanged;
    
    private int x, y;
    private int width, height;
    
    private final String tag;
    
    private boolean simulateUpdateDelay;
    
    private final int updatesInASecond;
    
    private BasicFixedDebug debugger;
    
    public static enum State{STANDING_AROUND, MOVING_TOWARDS_DESTINATION}
    
    public InputMixedGameEntity(String tag, int startX, int startY, int width, int height, boolean simulateUpdateDelay) {
        animationImages = BallClockTools.loadAnimationImages();
        this.x = startX;
        this.y = startY;
        this.width = width;
        this.height = height;
        this.tag = tag;
        this.updatesInASecond = 1000/InputMixedGameLoop.FIXED_STEP_IDEAL_DURATION_MS;
        debugger = new BasicFixedDebug();
        this.simulateUpdateDelay = simulateUpdateDelay;
    }
    
    public InputMixedGameEntity(String tag, int startX, int startY, int width, int height) {
        this(tag, startX, startY, width, height, false);
    }
    
    public void update() {
        loopsSinceLastImageIdxChanged = loopsSinceLastImageIdxChanged+1;
        if(loopsSinceLastImageIdxChanged==updatesInASecond) {
            currentAnimationImageIdx++;
            currentAnimationImageIdx = currentAnimationImageIdx%10;//wrap animation around
            loopsSinceLastImageIdxChanged=0;
        }
        
        debugger.debug(tag, currentAnimationImageIdx);
    }
    
    Sprite getCurrentSprite() {
        if(simulateUpdateDelay && currentAnimationImageIdx==7) {
            try {
                Thread.sleep(1500);
            } catch(Throwable t) {}
        }
        return new Sprite(this.x-this.width/2, this.y-this.height/2,
            this.width, this.height,
            animationImages[currentAnimationImageIdx]);
    }
    
    public void processInput(MouseEvent e) {
        move(e.getX(), e.getY());
    }
    
    private void move(int destinationX, int destinationY) {
        this.x = destinationX;
        this.y = destinationY;
    }
    
    public class Sprite {
        public final int x,y,width,height;
        public final BufferedImage image;
        
        public Sprite(int x, int y, int width, int height, BufferedImage image) {
            this.x = x;
            this.y = y;
            this.width = width;
            this.height = height;
            this.image = image;
        }        
    }
}
