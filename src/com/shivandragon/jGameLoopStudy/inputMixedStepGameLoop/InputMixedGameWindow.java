package com.shivandragon.jGameLoopStudy.inputMixedStepGameLoop;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.concurrent.ArrayBlockingQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;

import com.shivandragon.jGameLoopStudy.common.MeterPanel;
import com.shivandragon.jGameLoopStudy.inputMixedStepGameLoop.InputMixedGameEntity.Sprite;


@SuppressWarnings("serial")
public class InputMixedGameWindow extends JFrame implements MouseListener {

    private static final int WINDOW_SIZE = 400;
    
    private JPanel gamePanel;
    private MeterPanel stateUpsMeterPanel;
    private MeterPanel framesUpsMeterPanel;
    
    private InputMixedGameEntity[] protoGameEntities;
    
    private ArrayBlockingQueue<MouseEvent> inputQueue;
    
    public InputMixedGameWindow(InputMixedGameEntity... protoGameEntities) {
        gamePanel = new JPanel();
        gamePanel.setPreferredSize(new Dimension(WINDOW_SIZE, WINDOW_SIZE));
        gamePanel.setBackground(Color.GRAY);
        
        stateUpsMeterPanel = new MeterPanel("UPS");
        gamePanel.add(stateUpsMeterPanel);
        
        framesUpsMeterPanel = new MeterPanel("FPS");
        gamePanel.add(framesUpsMeterPanel);
        
        this.getContentPane().add(gamePanel);
        this.pack();
        this.setVisible(true);
        this.setResizable(false);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setTitle("Input Mixed Step Game Loop");
        
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width-this.getSize().width-dim.width/80, dim.height-this.getSize().height-dim.height/15);
        
        this.protoGameEntities = protoGameEntities;
        
        inputQueue = new ArrayBlockingQueue<MouseEvent>(5);
        
        this.addMouseListener(this);
    }
    
    public void processInput() {
    	while(!inputQueue.isEmpty()) {
	        MouseEvent e = inputQueue.poll();
	        for(InputMixedGameEntity entity:protoGameEntities) {
	        	entity.processInput(e);   
	        }
    	}
    }
    
    public void updateState() {
        stateUpsMeterPanel.onUpdate();
        
        processInput();
        for(InputMixedGameEntity entity:protoGameEntities) {
            entity.update();            
        } 
    }
    
    public void updateGraphics() {
        framesUpsMeterPanel.onUpdate();
        for(InputMixedGameEntity entity:protoGameEntities) {
            Sprite sprite = entity.getCurrentSprite();
            gamePanel.getGraphics().drawImage(sprite.image, sprite.x, sprite.y, sprite.width, sprite.height, null);
            gamePanel.repaint();
        }
    }

    public void mouseClicked(MouseEvent e) {
        inputQueue.offer(e);
    }
    
    public void mouseReleased(MouseEvent e) {
        inputQueue.offer(e);
    }

    public void mousePressed(MouseEvent pE) {}
    public void mouseEntered(MouseEvent pE) {}
    public void mouseExited(MouseEvent pE) {}
}
