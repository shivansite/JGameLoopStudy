package com.shivandragon.jGameLoopStudy.inputMixedStepGameLoop;




public class InputMixedGameLoop {
    
    public static final int FIXED_STEP_IDEAL_DURATION_MS = 1000/5; //(hope for) 5 updates per second
//    public static final int FIXED_STEP_IDEAL_DURATION_MS = 1000/100; //(hope for) 100 updates per second
//    public static final int FIXED_STEP_IDEAL_DURATION_MS = 1000/200; //(hope for) 200 updates per second
    
    
    public static void start() throws InterruptedException {
        InputMixedGameEntity ballClockGameEntity1 = new InputMixedGameEntity("Clock1", 0,0, 128, 128);
       
        InputMixedGameWindow mainWindow = new InputMixedGameWindow(ballClockGameEntity1);
        
        long accumulatedTimeMs = 0;
        while(true) {
            long startTime = System.currentTimeMillis();
            
            mainWindow.updateGraphics();
            while(accumulatedTimeMs >= FIXED_STEP_IDEAL_DURATION_MS) {
                mainWindow.updateState();
                accumulatedTimeMs -= FIXED_STEP_IDEAL_DURATION_MS;
            }
            
            long endTime = System.currentTimeMillis();
            accumulatedTimeMs += endTime - startTime;
        }
    }
    
    public static void main(String[] args) throws InterruptedException {
        start();
    }
    
}
