package com.shivandragon.jGameLoopStudy.basicFixedStepGameLoop;



public class BasicFixedGameLoop {
    
    public static final int FIXED_STEP_IDEAL_DURATION_MS = 1000/25; //(hope for) 25 updates per second
//    public static final int FIXED_STEP_IDEAL_DURATION_MS = 1000/100; //(hope for) 100 updates per second
//    public static final int FIXED_STEP_IDEAL_DURATION_MS = 1000/200; //(hope for) 200 updates per second
    
    public static void start() throws InterruptedException {
        BasicFixedGameEntity ballClockGameEntity1 = new BasicFixedGameEntity("Clock1", 0,0, 128, 128);
        BasicFixedGameEntity ballClockGameEntity2 = new BasicFixedGameEntity("Clock2", 50,200, 150, 150);
        BasicFixedGameEntity ballClockGameEntity3 = new BasicFixedGameEntity("Clock3", 300,200, 110, 110);
//        BasicFixedGameEntity ballClockGameEntity4 = new BasicFixedGameEntity("Clock4", 300,100, 120, 120);
        BasicFixedGameEntity ballClockGameEntity4 = new BasicFixedGameEntity("Clock4", 300,100, 120, 120, true);
        BasicFixedGameWindow mainWindow = new BasicFixedGameWindow(ballClockGameEntity1, 
            ballClockGameEntity2,
            ballClockGameEntity3,
            ballClockGameEntity4);
                
        long loopActualDurationMs = 0;
        
        /*
         * This game loop will globally lag behind if its previous step(s) have taken 
         * longer than the desired step duration to execute  
         */
        while(true) {
            long st = System.currentTimeMillis();
            mainWindow.update();
            long et = System.currentTimeMillis();
            loopActualDurationMs = et-st;
            
            long sleepTimeActualMs = FIXED_STEP_IDEAL_DURATION_MS-loopActualDurationMs;
            if(sleepTimeActualMs<=0) {
                //System.out.println("Calculated sleep time is negative ("+sleepTimeActualMs+" ms.) will not sleep this update.");
            } else {
               Thread.sleep(sleepTimeActualMs);
            }            
        }
    }
    
    public static void main(String[] args) throws InterruptedException {
        start();
    }
    
}
