package com.shivandragon.jGameLoopStudy.basicFixedStepGameLoop;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JPanel;

import com.shivandragon.jGameLoopStudy.basicFixedStepGameLoop.BasicFixedGameEntity.Sprite;
import com.shivandragon.jGameLoopStudy.common.MeterPanel;


@SuppressWarnings("serial")
public class BasicFixedGameWindow extends JFrame {

    private static final int WINDOW_SIZE = 400;
    
    private JPanel gamePanel;
    private MeterPanel upsMeterPanel;
    
    private BasicFixedGameEntity[] protoGameEntities;
    
    public BasicFixedGameWindow(BasicFixedGameEntity... protoGameEntities) {
        gamePanel = new JPanel();
        gamePanel.setPreferredSize(new Dimension(WINDOW_SIZE, WINDOW_SIZE));
        gamePanel.setBackground(Color.GRAY);
        
        upsMeterPanel = new MeterPanel("UPS (FPS)");
        gamePanel.add(upsMeterPanel);
        
        this.getContentPane().add(gamePanel);
        this.pack();
        this.setVisible(true);
        this.setResizable(false);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setTitle("Basic Fixed Step Game Loop");
        
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width-this.getSize().width-dim.width/80, dim.height-this.getSize().height-dim.height/15);
        
        this.protoGameEntities = protoGameEntities;
    }
    
    public void update() {
        upsMeterPanel.onUpdate();
        for(BasicFixedGameEntity entity:protoGameEntities) {
            entity.update();
            Sprite sprite = entity.getCurrentSprite();
            gamePanel.getGraphics().drawImage(sprite.image, sprite.x, sprite.y, sprite.width, sprite.height, null);
        } 
    }
}
